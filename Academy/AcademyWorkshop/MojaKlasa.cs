﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AcademyWorkshop
{
    class MojaKlasa
    {
        public event EventHandler OnPropertyChanged;
        string name = "";
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                    OnPropertyChanged(this, new EventArgs());
            }
        }
    }
}
